-- Piotr Zejler 116237 Sprawozdanie 2

DROP PROCEDURE wykonaj_przelad;
DROP PROCEDURE ile_wlascicieli;
DROP TYPE T_UtrataWartosci;
DROP TYPE T_Cena;
DROP TABLE Samochod;
DROP TYPE T_HistoraWlascicieli;
DROP TYPE T_Wlasciciel;
DROP TYPE T_Przeglady;
DROP TYPE T_Samochod;
DROP PACKAGE Package_it;
DROP TYPE T_OpcjeSamochodu;
DROP TABLE Rower;
DROP TYPE T_Rower;
DROP TYPE T_Pojazd;

-- Utorzenie typu T_Pojazd, posiada klauzulę ponieważ będą po nim dziedziczyć inne typy
-- Posiada procedurę umozliwiającą zmianę nazwy marki i modelu
-- Posiada funkcję pozwalajaca wyświetlić markę, model, rozmiar kół i masę
CREATE OR REPLACE TYPE T_Pojazd AS OBJECT
(
    marka VARCHAR2(50),
    model VARCHAR2(50),
    rozmiar_kol NUMBER(2),
    masa NUMBER(4),
    MEMBER PROCEDURE marka_model(marka_ VARCHAR2, model_ VARCHAR2),
    MEMBER FUNCTION wyswietl RETURN VARCHAR2
) NOT FINAL;
/
CREATE OR REPLACE TYPE BODY T_Pojazd AS 
    MEMBER PROCEDURE marka_model(marka_ VARCHAR2, model_ VARCHAR2) AS
    BEGIN
        SELF.marka := marka_;
        SELF.model := model_;
    END;
    MEMBER FUNCTION wyswietl RETURN VARCHAR2 IS
    BEGIN
        RETURN 'Marka: ' || marka || ' Model: ' || model || ' Rozmiar kół: ' 
        || rozmiar_kol || ' Masa: ' || masa;
    END;
END;
/

-- Utorzenie typu T_Rower, który dziedziczy po typie T_Pojazd
-- Typ posiada własny konstruktor, który w zależności od masy ustala rodzaj ramy roweru
-- Typ posiada metodę statyczną, zwracającą słowo Rower
CREATE OR REPLACE TYPE T_Rower UNDER T_Pojazd
(
    przerzutki NUMBER(2),
    rodzaj_ramy VARCHAR2(20),
    STATIC FUNCTION func_stat RETURN VARCHAR2,
    CONSTRUCTOR FUNCTION T_Rower(marka VARCHAR2, model VARCHAR2, rozmiar_kol NUMBER, masa NUMBER, przerzutki NUMBER)
        RETURN SELF AS RESULT
)
/
CREATE OR REPLACE TYPE BODY T_Rower AS 
    CONSTRUCTOR FUNCTION T_Rower(marka VARCHAR2, model VARCHAR2, 
    rozmiar_kol NUMBER, masa NUMBER, przerzutki NUMBER)
        RETURN SELF AS RESULT
    AS
    BEGIN
        SELF.marka := marka;
        SELF.model := model;
        SELF.rozmiar_kol := rozmiar_kol;
        SELF.masa := masa;
        SELF.przerzutki := przerzutki;
        SELF.rodzaj_ramy := rodzaj_ramy;
        
        IF masa >= 10 AND masa < 20 THEN
                SELF.rodzaj_ramy := 'Aluminiowa';
            ELSIF  masa < 10  THEN
                SELF.rodzaj_ramy := 'Karbonowa';
            ELSE
                SELF.rodzaj_ramy := 'Stalowa';
            END IF;
        RETURN;    
    END;
    STATIC FUNCTION func_stat RETURN VARCHAR2 IS
    BEGIN
        RETURN 'Rower';
    END;
END;
/

-- Utworzenie tabeli Rower na podstawie typu T_Rower
CREATE TABLE Rower OF T_Rower;

-- Utorzenie typu T_OpcjeSamochodu
-- Posiada procedurę pozwalajaca zmienić silnik
-- Posiada funkcję zwracająca możliwe do wyboru wyposażenie auta
CREATE OR REPLACE TYPE T_OpcjeSamochodu AS OBJECT
(
    wyposazenie VARCHAR2(50),
    rodzaj_silnika VARCHAR2(50),
    MEMBER PROCEDURE zmien_wyposazenie(wyposazenie_ VARCHAR2),
    MEMBER FUNCTION wyswietl RETURN VARCHAR2,
    MEMBER FUNCTION dostepne_wyposazenie RETURN VARCHAR2
)
/
CREATE OR REPLACE TYPE BODY T_OpcjeSamochodu AS 
    MEMBER PROCEDURE zmien_wyposazenie(wyposazenie_ VARCHAR2) AS
    BEGIN
        SELF.wyposazenie := wyposazenie_;
    END;
    MEMBER FUNCTION wyswietl RETURN VARCHAR2 IS
    BEGIN
        RETURN 'Wyposażenie: ' || wyposazenie || ' Rodzaj silnika: ' || rodzaj_silnika;
    END;
    MEMBER FUNCTION dostepne_wyposazenie RETURN VARCHAR2 IS
    BEGIN
        RETURN 'Dostępne wyposażenie: SPORT, PREMIUM, STANDARD';
    END;
END;
/

-- Pakiet wykorzystywany do przechowywania informacji o ilości stworzonych obiektów typu T_Samochod3
CREATE OR REPLACE PACKAGE Package_it IS
    iterator NUMBER := 0;
END;
/
-- Utworzenie typu T_Samochod, dziedziczący po T_Pojazd, wykorzystujący typ obiektowy T_OpcjeSamochodu
-- T_Samochod posiad konstruktor obliczający obecną wartość samochodu na podstawie tego ile samochód ma lat (takie coś sobie wymyśliłem)
-- oraz ustalający rozmiar felg na podstawie wyposażenia auta (rodzaje wyposażenia SPORT, PREMIUM, inne)
-- Metode statyczną, zwracającą słowo Samochód
-- Metode statyczną zwracającą informację o ilości obiektów tego typu
CREATE OR REPLACE TYPE T_Samochod UNDER T_Pojazd
(
    kolor VARCHAR2(50),
    rok_produkcji NUMBER(4), 
    opcje T_OpcjeSamochodu,
    cena_nowego REAL,
    cena_obecna REAL,
    STATIC FUNCTION func_stat1 RETURN VARCHAR2,
    STATIC FUNCTION func_stat2 RETURN NUMBER,

    CONSTRUCTOR FUNCTION T_Samochod(marka VARCHAR2, model VARCHAR2, masa NUMBER, 
    kolor VARCHAR2, rok_produkcji NUMBER, opcje T_OpcjeSamochodu, cena_nowego REAL)
        RETURN SELF AS RESULT
)
/

CREATE OR REPLACE TYPE BODY T_Samochod AS
    CONSTRUCTOR FUNCTION T_Samochod(marka VARCHAR2, model VARCHAR2, masa NUMBER, 
    kolor VARCHAR2, rok_produkcji NUMBER, opcje T_OpcjeSamochodu, cena_nowego REAL)
        RETURN SELF AS RESULT
    AS
    BEGIN
        SELF.marka := marka;
        SELF.model := model;
        SELF.masa := masa;
        SELF.kolor := kolor;
        SELF.rok_produkcji := rok_produkcji;
        SELF.opcje := opcje;
        SELF.cena_nowego := cena_nowego;
        SELF.cena_obecna := ROUND(cena_nowego - (cena_nowego * (0.05 * 
            (EXTRACT(YEAR FROM CURRENT_DATE) - rok_produkcji))));
        
        IF UPPER(opcje.wyposazenie) = 'SPORT' THEN
                SELF.rozmiar_kol := 19;
            ELSIF UPPER(opcje.wyposazenie) = 'PREMIUM' THEN
                SELF.rozmiar_kol := 17;
            ELSE
                SELF.rozmiar_kol := 15;
            END IF;        
        RETURN;
    END;
    STATIC FUNCTION func_stat1 RETURN VARCHAR2 IS
    BEGIN
        RETURN 'Samochód';
    END;
    STATIC FUNCTION func_stat2 RETURN NUMBER IS
    BEGIN
        Package_it.iterator := Package_it.iterator + 1;
        RETURN Package_it.iterator;
    END;
END;
/

-- Utworzenie typu kolekcji VARRAY, która będzie wykorzystana w tabeli Samochod do przechowywania historii przeglądów pojazdu
CREATE OR REPLACE TYPE T_Przeglady AS VARRAY(25) OF DATE;
/

-- Typ wykorzystany w typie T_HistoraWlascicieli
CREATE OR REPLACE TYPE T_Wlasciciel AS OBJECT
(
    imie VARCHAR2(50), 
    nazwisko VARCHAR2(50), 
    kraj VARCHAR(50)
);

/

-- Utworzenie typu kolekcji NESTED TABLE, która będzie wykorzystana w tabeli Samochod do przechowywania historii właścicieli pojazdu
CREATE OR REPLACE TYPE T_HistoraWlascicieli AS TABLE OF T_Wlasciciel;
/

-- Utworzenie tabeli i wykorzystanie T_Samochod jako COLUMN OBJECT
-- kolekcji VARRAY, w której przechowywane są daty przeglądów
-- kolekcji NESTED TABLE, w której przechowywane są imie, nazwisko i kraj wszystkich właścicieli, którzy posiadali auto
CREATE TABLE Samochod
(
    p_samochod T_Samochod,
    numer_rejestracyjny VARCHAR2(7) UNIQUE,
    wlasciciele T_HistoraWlascicieli,
    przeglady T_Przeglady
) NESTED TABLE wlasciciele STORE AS nested_wlasciciele;
/

-- Utworzenie typu T_Cena, wykorzytujący typ T_Samochod oraz zawierajacy metodę ORDER MEMBER, do porównania ceny dwóch samochodów
CREATE OR REPLACE TYPE T_Cena AS OBJECT
(
    samochod T_Samochod,
    ORDER MEMBER FUNCTION cena (c T_Cena) RETURN INTEGER 
)
/
CREATE OR REPLACE TYPE BODY T_Cena AS
    ORDER MEMBER FUNCTION cena (c T_Cena) RETURN INTEGER IS
    BEGIN
        IF samochod.cena_obecna < c.samochod.cena_obecna THEN
            RETURN -1;
        ELSIF samochod.cena_obecna > c.samochod.cena_obecna THEN
            RETURN 1;
        ELSE
            RETURN 0;
        END IF;
    END;
END;
/

-- Utworzenie typu T_UtrataWartosci, wykorzytujący typ T_Samochod oraz zawierajacy metodę ORDER MEMBER, do porównania utraty wartości dwóch samochodów
CREATE OR REPLACE TYPE T_UtrataWartosci AS OBJECT
(
    samochod T_Samochod,
    ORDER MEMBER FUNCTION utrata (u T_UtrataWartosci) RETURN INTEGER 
)
/
CREATE OR REPLACE TYPE BODY T_UtrataWartosci AS
    ORDER MEMBER FUNCTION utrata (u T_UtrataWartosci) RETURN INTEGER IS
    BEGIN
        IF (100 - (samochod.cena_obecna * 100) / samochod.cena_nowego) < 
            (100 - (u.samochod.cena_obecna * 100) / u.samochod.cena_nowego) THEN
                RETURN -1;
        ELSIF (100 - (samochod.cena_obecna * 100) / samochod.cena_nowego) > 
            (100 - (u.samochod.cena_obecna * 100) / u.samochod.cena_nowego) THEN
                RETURN 1;
        ELSE
            RETURN 0;
        END IF;
    END;
END;
/


-- Procedura sprawdzająca ile właścicieli miał samochód oraz w jakim kraju został po raz pierwszy zarejestrowany
-- jako argument podajemy numer rejestracyjny pojazdu
CREATE OR REPLACE PROCEDURE ile_wlascicieli(rej VARCHAR2)
IS 
    tabWlascicieli T_HistoraWlascicieli;
    pierwszy_wlasciciel T_Wlasciciel;
    ile NUMBER(2);
BEGIN
    SELECT s.wlasciciele INTO tabWlascicieli FROM samochod s WHERE s.numer_rejestracyjny = rej;
    pierwszy_wlasciciel := tabWlascicieli(1);
    ile := tabWlascicieli.COUNT;
    dbms_output.put_line('Samochod pochodzi z ' || pierwszy_wlasciciel.kraj);
    dbms_output.put_line('Jego historia wykazuje, że miał ' || ile || ' właścicieli');
END;
/

-- Dodanie nowego przeglądu do samochodu 
-- jako argument podajemy numer rejestracyjny pojazdu i opcjonalnie datę, jeśli jej nie podamy to wpisze się dzisiejsza data
CREATE OR REPLACE PROCEDURE wykonaj_przelad(rej VARCHAR2, data_przegladu DATE DEFAULT CURRENT_DATE)
IS
    tabPrzeglady T_Przeglady;
    ile NUMBER(3);
BEGIN
    SELECT s.przeglady INTO tabPrzeglady FROM samochod s WHERE s.numer_rejestracyjny = rej;
    tabPrzeglady.EXTEND(1);
    ile := tabPrzeglady.COUNT;
    tabPrzeglady(ile) := data_przegladu;
    UPDATE samochod SET przeglady =  tabPrzeglady  WHERE numer_rejestracyjny = rej;
    dbms_output.put_line('Dodano nowy przegląd do samochodu o numerze rejestracyjnym ' 
    || rej || ' (' || tabPrzeglady(ile) || ')');
END;
/

-------------------------------------------------UZUPEŁNIANIE TABEL------------------------------------------------------------------------------------
-- Uzupełnienie tabeli Rower danymi
-- Dodawanie danych do tabeli OpcjeSamochodu bez użycia konstruktora
INSERT INTO Rower VALUES('Scout', 'Dx120', 26, 11, 21, 'Aluminiowa');
INSERT INTO Rower VALUES('Atletics', 'RW900', 27, 12, 11, 'Aluminiowa');
INSERT INTO Rower VALUES('Porsche', 'P910', 26, 6, 23, 'Karbonowa');
INSERT INTO Rower VALUES('Scout', 'Ds90', 21, 21, 18, 'Stalowa');
INSERT INTO Rower VALUES('Atletics', 'RS180', 24, 21, 20, 'Stalowa');

-- Dodawanie danych do tabeli OpcjeSamochodu z użyciem konstruktora
INSERT INTO Rower VALUES(T_Rower('Scout', 'Dx120', 28, 14, 18));
INSERT INTO Rower VALUES(T_Rower('Atletics', 'RS540', 23, 9, 28));
INSERT INTO Rower VALUES(T_Rower('Porsche', 'P809', 26, 5, 21));
INSERT INTO Rower VALUES(T_Rower('Atletics', 'RS090', 22, 22, 19));
INSERT INTO Rower VALUES(T_Rower('Scout', 'Dx768', 18, 12, 25));


-- Uzupełnienie tabeli Samochód danymi
INSERT INTO Samochod VALUES
(
    T_Samochod('Honda', 'Civic', 1800, 'Czerwony', 2012, T_OpcjeSamochodu('Sport', 'Benzyna'), 80000), 
    'SC12345', 
    T_HistoraWlascicieli(T_Wlasciciel('John', 'Smith', 'USA'),
                         T_Wlasciciel('Michał', 'Kowalski', 'Polska'),
                         T_Wlasciciel('Anna', 'Nowak', 'Polska')),
    T_Przeglady(TO_DATE ('2013-11-25','YYYY-MM-DD'),
                TO_DATE ('2014-11-29','YYYY-MM-DD'),
                TO_DATE ('2015-11-22','YYYY-MM-DD'),
                TO_DATE ('2016-11-30','YYYY-MM-DD'))
);

INSERT INTO Samochod VALUES
(
    T_Samochod('Mazda', '5', 1900, 'Niebiieski', 2010, T_OpcjeSamochodu('Premium', 'Diesel'), 981500), 
    'SC09876', 
    T_HistoraWlascicieli(T_Wlasciciel('Tomasz', 'Zawadzki', 'Polska')),
    T_Przeglady(TO_DATE ('2011-10-17','YYYY-MM-DD'),
                TO_DATE ('2012-10-11','YYYY-MM-DD'),
                TO_DATE ('2013-10-13','YYYY-MM-DD'),
                TO_DATE ('2014-10-09','YYYY-MM-DD'),
                TO_DATE ('2015-10-11','YYYY-MM-DD'),
                TO_DATE ('2016-10-12','YYYY-MM-DD'))
);
INSERT INTO Samochod VALUES
(
    T_Samochod('VW', 'Polo', 1500, 'Srebrny', '2003', T_OpcjeSamochodu('Standard', 'Benzyna'), 54000), 
    'SC76543', 
    T_HistoraWlascicieli(T_Wlasciciel('Thomas', 'Muller', 'Niemcy'),
                         T_Wlasciciel('Paweł', 'Janas', 'Polska'),
                         T_Wlasciciel('Monika', 'Klec', 'Polska'),
                         T_Wlasciciel('Paweł', 'Ślęzak', 'Polska')),
    T_Przeglady(TO_DATE ('2004-02-01','YYYY-MM-DD'),
                TO_DATE ('2005-02-05','YYYY-MM-DD'),
                TO_DATE ('2006-02-05','YYYY-MM-DD'),
                TO_DATE ('2007-02-03','YYYY-MM-DD'),
                TO_DATE ('2008-02-01','YYYY-MM-DD'),
                TO_DATE ('2009-02-03','YYYY-MM-DD'),
                TO_DATE ('2010-02-03','YYYY-MM-DD'),
                TO_DATE ('2011-02-05','YYYY-MM-DD'),
                TO_DATE ('2012-02-01','YYYY-MM-DD'),
                TO_DATE ('2013-02-03','YYYY-MM-DD'),
                TO_DATE ('2014-02-03','YYYY-MM-DD'),
                TO_DATE ('2015-02-01','YYYY-MM-DD'),
                TO_DATE ('2016-02-05','YYYY-MM-DD'),
                TO_DATE ('2017-02-01','YYYY-MM-DD'))
);
INSERT INTO Samochod VALUES
(
    T_Samochod('Mercedes', 'Vito', 1760, 'Czerowony', 2016, T_OpcjeSamochodu('Standard', 'Benzyna'), 67800), 
    'SC63780', 
    T_HistoraWlascicieli(T_Wlasciciel('Wiktor', 'Kowal', 'Polska')),
    T_Przeglady(TO_DATE ('2017-01-25','YYYY-MM-DD'))
);
INSERT INTO Samochod VALUES
(
    T_Samochod('Fiat', 'Tipo', 1490, 'Biały', 2015, T_OpcjeSamochodu('Standard', 'Diesel'), 56400), 
    'SC83957', 
    T_HistoraWlascicieli(T_Wlasciciel('Antoni', 'Zawada', 'Polska')),
    T_Przeglady(TO_DATE ('2016-05-14','YYYY-MM-DD'),
                TO_DATE ('2017-05-19','YYYY-MM-DD'))
);
INSERT INTO Samochod VALUES
(
    T_Samochod('Seat', 'Ibiza', 1300, 'Czarny', 2004, T_OpcjeSamochodu('Sport', 'Benzyna'), 65000), 
    'SC93659', 
    T_HistoraWlascicieli(T_Wlasciciel('Adam', 'Tomczyk', 'Polska'),
                         T_Wlasciciel('Michał', 'Janicki', 'Polska'),
                         T_Wlasciciel('Paweł', 'Kropka', 'Polska'),
                         T_Wlasciciel('Piotr', 'Zawadzki', 'Polska'),
                         T_Wlasciciel('Anna', 'Kmiec', 'Polska')),
    T_Przeglady(TO_DATE ('2004-11-25','YYYY-MM-DD'),
                TO_DATE ('2005-11-29','YYYY-MM-DD'),
                TO_DATE ('2006-11-24','YYYY-MM-DD'),
                TO_DATE ('2007-11-22','YYYY-MM-DD'),
                TO_DATE ('2008-11-21','YYYY-MM-DD'),
                TO_DATE ('2009-11-20','YYYY-MM-DD'),
                TO_DATE ('2010-11-24','YYYY-MM-DD'),
                TO_DATE ('2011-11-26','YYYY-MM-DD'),
                TO_DATE ('2012-11-19','YYYY-MM-DD'),
                TO_DATE ('2013-11-29','YYYY-MM-DD'),
                TO_DATE ('2014-11-28','YYYY-MM-DD'),
                TO_DATE ('2015-11-25','YYYY-MM-DD'),
                TO_DATE ('2016-11-22','YYYY-MM-DD'))
);
INSERT INTO Samochod VALUES
(
    T_Samochod('Toyota', 'Aygo', 980, 'Czerowony', 2015, T_OpcjeSamochodu('Standard', 'Diesel'), 38000), 
    'SC73696', 
    T_HistoraWlascicieli(T_Wlasciciel('Ana', 'Klopp', 'Niemcy'),
                         T_Wlasciciel('Paweł', 'Miras', 'Polska')),
    T_Przeglady(TO_DATE ('2016-09-22','YYYY-MM-DD'),
                TO_DATE ('2017-09-23','YYYY-MM-DD'))
);
INSERT INTO Samochod VALUES
(
    T_Samochod('BMW', '5', 2100, 'Srebrny', 2016, T_OpcjeSamochodu('Sport', 'Benzyna'), 250600), 
    'SC93864', 
    T_HistoraWlascicieli(T_Wlasciciel('Sławomir', 'Grabowski', 'Polska')),
    T_Przeglady(TO_DATE ('2017-04-11','YYYY-MM-DD'))
);
INSERT INTO Samochod VALUES
(
    T_Samochod('Fiat', 'Panda', 1100, 'Czerowony', 2012, T_OpcjeSamochodu('Standard', 'Diesel'), 40090), 
    'SC63798', 
    T_HistoraWlascicieli(T_Wlasciciel('Ignacy', 'Nowacki', 'Polska'),
                         T_Wlasciciel('Jarosław', 'Truszcz', 'Polska')),
    T_Przeglady(TO_DATE ('2013-01-25','YYYY-MM-DD'),
                TO_DATE ('2014-01-29','YYYY-MM-DD'),
                TO_DATE ('2015-01-29','YYYY-MM-DD'),
                TO_DATE ('2016-01-22','YYYY-MM-DD'),
                TO_DATE ('2017-01-30','YYYY-MM-DD'))
);
INSERT INTO Samochod VALUES
(
    T_Samochod('Peugeot', '309', 1250, 'Czarny', 2015, T_OpcjeSamochodu('SPORT', 'Benzyna'), 69800), 
    'SC15748', 
    T_HistoraWlascicieli(T_Wlasciciel('Monika', 'Zakrzewska', 'Polska')),
    T_Przeglady(TO_DATE ('2015-03-12','YYYY-MM-DD'),
                TO_DATE ('2016-03-19','YYYY-MM-DD'),
                TO_DATE ('2017-03-20','YYYY-MM-DD'))
);


-------------------------------------------------TESTOWANIE------------------------------------------------------------------------------------
-- Wyświetlenie tabeli Rower
SELECT * FROM Rower;

SELECT T_Rower.func_stat() POJAZD, marka, model, masa, przerzutki, rodzaj_ramy, rozmiar_kol FROM Rower ORDER BY masa DESC;

-- Wyświetlenie tabeli Samochod
SELECT *  FROM Samochod;

SELECT T_Samochod.func_stat1() POJAZD, T_Samochod.func_stat2() IT_OBJ, TREAT(o.p_samochod AS T_Samochod).marka MARKA , TREAT(o.p_samochod AS T_Samochod).model "MODEL", 
TREAT(o.p_samochod AS T_Samochod).rozmiar_kol "ROZMIAR KÓŁ", TREAT(o.p_samochod AS T_Samochod).masa MASA,
TREAT(o.p_samochod AS T_Samochod).rok_produkcji ROCZNIK, TREAT(o.p_samochod AS T_Samochod).opcje.wyposazenie WYPOSAŻENIE,
TREAT(o.p_samochod AS T_Samochod).opcje.rodzaj_silnika SILNIK, TREAT(o.p_samochod AS T_Samochod).cena_nowego "CENA NOWEGO",
TREAT(o.p_samochod AS T_Samochod).cena_obecna "CENA OBECNA",
numer_rejestracyjny, przeglady, wlasciciele FROM Samochod o;


-- Przetesowanie procedury i funkcji w typie T_Pojazd
DECLARE 
    pojazd T_Pojazd;
BEGIN 
    pojazd := T_Pojazd('Audi', 'A6', 18, 1900);
    DBMS_OUTPUT.PUT_LINE(pojazd.wyswietl);
    
    pojazd.marka_model('BMW', '5');
    DBMS_OUTPUT.PUT_LINE(pojazd.wyswietl);
END;
/

-- Przetesowanie procedury i funkcji w typie T_OpcjeSamochodu
DECLARE 
    opcje T_OpcjeSamochodu;
BEGIN 
    opcje := T_OpcjeSamochodu('Standard', 'Diesel');
    DBMS_OUTPUT.PUT_LINE(opcje.wyswietl);
    DBMS_OUTPUT.PUT_LINE(opcje.dostepne_wyposazenie);
    opcje.zmien_wyposazenie('Premium');
    DBMS_OUTPUT.PUT_LINE(opcje.wyswietl);
END;
/

-- Wywołanie funkcji ORDER MEMBER
-- Porównanie cen dwóch samochodów
DECLARE 
    auto1 T_Cena;
    auto2 T_Cena;
    a NUMBER;
BEGIN
    auto1 := NEW T_Cena(T_Samochod('Audi', 'A4', 1800, 'Czerwony', 2009, T_OpcjeSamochodu('PREMIUM', 'Diesel'), 180000));
    auto2 := NEW T_Cena(T_Samochod('Toyota', 'Yaris', 1300, 'Czarny', 2014, T_OpcjeSamochodu('SPORT', 'Benzyna'), 60000));
    a := auto1.cena(auto2);
    DBMS_OUTPUT.PUT_LINE('Samochód jest (1 droższy, -1 tańszy): ' ||a);
END;
/

-- Porównanie, który samochód stracił więcej na wartości
DECLARE 
    auto1 T_UtrataWartosci;
    auto2 T_UtrataWartosci;
    a NUMBER;
BEGIN
    auto1 := NEW T_UtrataWartosci(T_Samochod('Audi', 'A4', 1800, 'Czerwony', 2009, T_OpcjeSamochodu('PREMIUM', 'Diesel'), 180000));
    auto2 := NEW T_UtrataWartosci(T_Samochod('Toyota', 'Yaris', 1300, 'Czarny', 2014, T_OpcjeSamochodu('SPORT', 'Benzyna'), 60000));
    a := auto1.utrata(auto2);
    DBMS_OUTPUT.PUT_LINE('Samochód stracił na wartości (1 więcej, -1 mniej): ' ||a);
END;
/

-- Wywołania procedur
-- Sprawdzenie ile właścicieli miał pojazd o numer rejestracyjnym SC12345 oraz z jakiego kraju pochodzi
exec ile_wlascicieli('SC12345');

-- Dodanie wybranej daty wykonania przeglądu do pojazdu o numerze rejestracyjnym SC12345
exec wykonaj_przelad('SC12345', '2017-03-20');

-- Dodanie dzisiejszej daty wykonania przeglądu do pojazdu o numerze rejestracyjnym SC12345
exec wykonaj_przelad('SC12345');

SELECT  numer_rejestracyjny, przeglady FROM Samochod where numer_rejestracyjny = 'SC12345';
